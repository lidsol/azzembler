import sys
class ArchivoEntrada:
    macros = {} # Contiene la MDT Int: String
    ala = {} # Contiene la ALA Int : (Char,Char)
    mnd = {} # Contiene la MND Int: (String, Int, Int)
    programa = " " # Programa sin MacroDefiniciones
    def __init__(self,nombre): # Constructor de la clase ArchivoEntrada
        # archivo de Entrada con extension .asm
        fname = nombre # Referencia al nombre del archivo
        self.abrir(fname) # Abre el archivo y se prepara para obtener las Macrodefiniciones
    def construirMND(self): # Metodo que construye la MND
        # Obtiene los datos de la Tabla de Nombres
        print("Obteniendo MND: ")
        contador = 1
        for i in self.macros:
            for j in self.ala:
                if str(self.ala[j]) in self.macros[i] :
                    nombre = self.macros[i]
                    n = nombre.split(":")
                    ndef = n[0]
                    #print(str(contador)+ " | " + ndef +" => "+ str(str(i) + " => " + str(j)))
                    elem = (ndef,i,j)
                    self.mnd[contador] = elem
                    contador += 1




    def construirALA(self,llamadas):
        #Con las llamadas crea la tabla ALA
        print("Obteiendo ALA: ")
        arr = self.macros.values()
        nt = {} # Diccionario Macro : Argumentos
        contador = 0 #Contador de ALA
        for a in arr:
            if "MACRO" in a:
                args = a.split("MACRO") # Obtiene por separado  los nombres
                # y los argumentos
                n= args[0].split(":") # Elimina los :
                nt[args[0]] = args[1] # Aumenta macros al diccionario
                #print (nt)
        lineas = llamadas.split("\n")
        for l in lineas:
            for names in nt.keys():
                namesc = names.split(":")
                argumentos = nt[names]
                arglen = len(argumentos.split(" "))-1
                if(namesc[0] in l):
                    argdef = l.split(" ")
                    cont = 0
                    lencall = len(argdef) -1
                    if lencall == arglen:
                        for t in argdef:
                            if (t == "" or t in namesc[0]):
                                contador += 1
                                self.ala[contador] = namesc[0]
                            else:
                                # print (t)
                                arrargs = str(nt[names])
                                L = arrargs.split(" ")
                                # print(L[cont + 1])
                                cont += 1
                                contador += 1
                                tupla = (t, L[cont])
                                self.ala[contador] = tupla
                    else:
                        sys.exit("Error Pocos Argumentos en: "+ l)
    # Fin de construir ALA
        self.construirMND()





    def getMacros(self,file): # Metodo que crea la MDT
        print("Obteniendo MDT: ")
        contador = 1 # Contador de lineas de codigo
        for l in file:
            if("MACRO" in l): # Obtiene el cuerpo de la MACRO
                defin = l.split("\n")
                for a in defin:
                    if( a != ""):
                     self.macros[contador] = a # Pone cada linea a la MDT
                     contador += 1 # Actualiza el contador
            else:
                self.programa = l # En caso de no ser parte de las macrodefinciones
                # Es parte del programa
        self.construirALA(self.programa) # El siguiente paso es construir la ALA
        self.programa = self.programa

    def abrir(self,nombre):
         print("Abriendo: " + nombre)
         archivo = open(nombre,"r") # Abre el archivo para lectura de MacroDefiniciones
         archivos = archivo.read().split("MEND") # Divide el archivo en Macrodefiniciones y
         # El resto del programa
         macrosx = len(archivos)-1
         cmacros = 0
         for l in archivos:
             if ("MACRO" in l):
                 cmacros = cmacros + 1
         if (cmacros == macrosx ):
             self.getMacros(archivos)
         else:
             sys.exit("Error De Sintaxis: Faltan Por Cerrar Macros")
