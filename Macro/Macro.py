# MacroEnsamblador del MicroProsesador Z80
# Leer Archivo - listo
# Obtener las MacroDefiniciones - listo
# Crear Las Tablas - listo
# Devolver el archivo sin MacroDefiniciones
# Librerias usadas
import sys # Para leer argumentos desde la terminal
import ArcEntada # Para crear Tablas
import ArchSalida # Para devolver el codigo fuente sin macros

programafuente = str(sys.argv[1]) # Obtiene la ruta del archivo fuente con MacroDefiniciones
Macro = ArcEntada.ArchivoEntrada(programafuente) # Inicia la ejecucion del programa
print("Bienvenido al MacroEnsamblador PRO Z80 ")
print("===================")
print("MDT")
print("CMDT Cuerpo")
for c in Macro.macros:
    print( str(c) + " | " +  str(Macro.macros[c]))

print("===================")
print("ALA")
print("CALA PF PA")
for c in Macro.ala:
    print(str(c) + " | " + str(Macro.ala[c]))
print("===================")
print("MND")
print("CMNT NOMBRE AMDT AALA")
for c in Macro.mnd:
    print(str(c) + "|" + str(Macro.mnd[c]))


programaSalida = (programafuente.split(".")[0])+"nm.asm" #Construye el nombre del archivo de salida
print("Construyendo: " + programaSalida)
salida = ArchSalida.ArchivoSalida(programaSalida,Macro.macros,Macro.ala,Macro.mnd,Macro.programa)