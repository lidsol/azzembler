class ArchivoSalida:
    def __init__(self,fname,mdt,ala,mnd,nomacros): # Constructor de la Clase Archivo Salida
        filename = fname
        programa = nomacro
        definiciones = mdt 
        argumentos = ala
        llamadas = mnd
        self.MakeAsm(programa, definiciones,argumentos,llamadas,filename)

    def MakeAsm(self,source,defs,args,calls,salida): #Funcion que crea el arcivo de salida
        sources = source.split("\n")
        fout = open(salida,"w")
        for linea in sources: # Itera en cada linea del programa con Macrollamadas
            llamadais = False
            topush = ""
            if ("END" in linea): #El programa termina hasta que se encuentra la directiva END
             break
            for llamadas in calls: # Itera en la MND para encontrar llamadas
                call = calls[llamadas][0] # Llamada en la MND
                if call in linea: #Si se encuentra en una llamada se sustituye por la MacroDefinicion en la MDT
                    aCuerpo = calls[llamadas][1] + 1 # Inicio de la MacroDefinicion
                    aArgs = calls[llamadas][2]+1 #Inico de los Argumentos
                    for ite in range(aCuerpo,len(defs)+1): #Itera sobre la tabla de MacroDefiniciones
                        if "MACRO" in defs[ite]: # Si se traslapa con otra Macro termina el ciclo
                            break
                        else:
                            if "#" in defs[ite]:
                                lnew = "" #Guarda una nueva linea
                                for ite2 in range(aArgs,len(args)+1):
                                    if "#" in args[ite2][1]:
                                        if args[ite2][1] in defs[ite]:
                                            new = defs[ite].replace(args[ite2][1],args[ite2][0])
                                            lnew = new

                                    else:
                                        break
                                topush = topush + lnew + "\n"
                            else:
                                topush = topush + defs[ite] +"\n"
                    llamadais = True
                    del calls[llamadas]
                    break
            if llamadais:
                print(topush)
                fout.write(topush+"")

            else:
                fout.write(linea+"\n")
                print(linea)
