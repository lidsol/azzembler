# Azzembler: otro macroensamblador para el z80
Este es el repositorio del z80 Azzembler.
Este proyecto tiene como objetivo crer un macroensamblador que pueda ser 
usado y estudiado en la clase de Estructura y Programación de Computadoras en
la Facultad de ingeniería de la UNAM.
El ensamblador esta escrito en python como un programa estructurado, para 
permitir analizar fácilmente su código y para seguir el algoritmo de 
ensamblaje enseñado por el profesor Templos Carbajal.

## Directrices del código:
* Código de python 2.7
* Nada de código orientado a objetos
* Indentación a 4 espacios, no usar tabuladores
* Todas las funciones deben ser documentadas usando el formato docstring de 
google
* Los nombres de variables y funciones deben de ser lo más descriptivos 
posible y reflejar exactamente su uso
* Abre un pull request hasta que tu código haya pasado todas las pruebas en 
el repo


# Azzembler: another Z80 macro assembler
This is the repository for the z80 Azzembler project.
This project aims to create an assembler for the z80 microprocessor, to use 
and study in the 1429 Computer Structure and Programming class at the FI UNAM.
It's written in python as a structured program, it is designed following the 
two pass assembly algorithm taught by professor Templos Carbajal.

## Code guidelines:
* Python 2.7 code only
* No object oriented code in the program
* Four spaces indents, no tabs
* All functions commented in google docstring format
* Names must be as descriptive as possible and reflect their use
* Don't pull request until your code has passed al the tests included in the 
repo
