#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2016 Emilio Cabrera
#
# GNU GENERAL PUBLIC LICENSE
#    Version 3, 29 June 2007
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import azz_error as azze
import parser
import azz_globals as azzg

# Valid Addressing Modes
# General Purpose Registers
r = {  # ld,
     "b": "000",
     "c": "001",
     "d": "010",
     "e": "011",
     "h": "100",
     "l": "101",
     "a": "111"
}
# Registers Pairs
qq = {
    "bc": "00",
    "de": "01",
    "hl": "10",
    "af": "11"
}
ss = {  # ld,
    "bc": "00",
    "de": "01",
    "hl": "10",
    "sp": "11"
}
pp = {
    "bc": "00",
    "de": "01",
    "ix": "10",
    "sp": "11"
}
rrr = {
    "bc": "00",
    "de": "01",
    "iy": "10",
    "sp": "11"
}
# Flag Register
cc = {
    "nz": "000",
    "z":  "001",
    "nc": "010",
    "c":  "011",
    "po": "100",
    "pe": "101",
    "p":  "110",
    "m":  "111"
}
# Interrupt
p = {
    "00000000": "000",
    "00001000": "001",
    "00010000": "010",
    "00011000": "011",
    "00100000": "100",
    "00101000": "101",
    "00110000": "110",
    "00111000": "111"
}

# Valid operands
v_ops = [
    "b",    "c",    "d",    "e",    "h",    "l",    "a",    "r",    "i"  # 8 bits
    "bc",   "de",   "hl",   "af",   "af'",  "sp",   "ix",   "iy",  # 16 bits
    "nz",   "z",    "nc",   "c",    "po",   "pe",   "p",    "m",  # jumps
    "(bc)", "(de)", "(hl)", "(ix+", "(iy+", "(c)",  # indirect register
]

# Machine Code Generation Routines

# Load Group


def ld(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 2:
        azze.few_args("ld")
    elif op_count > 2:
        azze.many_args("ld")
    else:  # addressing modes: r, ss, (hl), (bc), (de), (ix+d), (iy+d), ix,
        # iy, sp, i, r
        operands, ops_types, is_indirect = parser.detect_addressing_mode(
            operands)
        op1 = operands[0]
        mode1 = ops_types[0]
        op2 = operands[1]
        mode2 = ops_types[1]
        # 8 bits operations
        if op1 in r:
            if op1 == "a":
                if op2 == "(bc)":  # ld a, (bc)
                    opcode = "00001010"
                    size = 1
                elif op2 == "(de)":  # ld a, (de)
                    opcode = "00011010"
                    size = 1
                elif mode2 == "numeric" and is_indirect[1]:  # ld a, (nn)
                    opcode = "00111010" + op2[1:-1]
                    size = 3
                elif op2 == "i":  # ld a, i
                    opcode = "1110110101010111"
                    size = 2
                elif op2 == "r":  # ld a, r
                    opcode = "1110110101011111"
                    size = 2
            if op2 in r:  # ld r,r
                opcode = "01" + r[op1] + r[op2]
                size = 1
            elif mode2 == "numeric" and not is_indirect[1]:  # ld r, n
                opcode = "00" + r[op1] + "110" + op2
                size = 2
            elif op2 == "(hl)":  # ld r, (hl)
                opcode = "01" + r[op1] + "110"
                size = 1
            elif op2[:4] == "(ix+":  # ld r, (ix+d)
                opcode = "1101110101" + r[op1] + "110" + \
                         parser.get_displacement(op2)
                size = 3
            elif op2[:4] == "(iy+":  # ld r, (iy+d)
                opcode = "1111110101" + r[op1] + "110" + \
                         parser.get_displacement(op2)
                size = 3
            elif mode2 == "label" and not is_indirect[1] and is_first_pass:
                size = 2
            elif op1 == "a" and mode2 == "label" and is_indirect[1] and \
                    is_first_pass:
                size = 3
            else:
                azze.invalid_operand(op1)
        elif op1 == "(hl)":
            if op2 in r:  # ld (hl), r
                opcode = "01110" + r[op2]
                size = 1
            elif mode2 == "numeric" and not is_indirect[1]:  # ld (hl), n
                opcode = "00110110" + op2
                size = 2
            elif mode2 == "label" and not is_indirect[1] and is_first_pass:
                size = 2
            else:
                azze.invalid_operand(op2)
        elif op1[:4] == "(ix+":
            if op2 in r:  # ld (ix+d), r
                opcode = "1101110101110" + r[op2] + \
                         parser.get_displacement(op1)
                size = 3
            elif mode2 == "numeric" and not is_indirect[1]:
                # ld (ix+d), n
                opcode = "1101110100110110" + parser.get_displacement(
                    op1) + op2
                size = 4
            elif mode2 == "label" and not is_indirect[1] and is_first_pass:
                size = 4
            else:
                azze.invalid_operand(op2)
        elif op1[:3] == "(iy+":
            if op2 in r:  # ld (ix+d), r
                opcode = "1111110101110" + r[op2] + \
                         parser.get_displacement(op1)
                size = 3
            elif mode2 == "numeric" and not is_indirect[1]:
                # ld (ix+d), n
                opcode = "1111110100110110" + parser.get_displacement(
                    op1) + op2
                size = 4
            elif mode2 == "label" and not is_indirect[1] and is_first_pass:
                size = 4
            else:
                azze.invalid_operand(op2)
        elif op2 == "a":
            if op1 == "(bc)":  # ld (bc), a
                opcode = "00000010"
                size = 1
            elif op1 == "(de)":  # ld (de), a
                opcode = "00010010"
                size = 1
            elif mode1 == "numeric" and is_indirect[0]:  # ld (nn), a
                opcode = "00110010" + op1[1:-1]
                size = 3
            elif op1 == "i":  # ld i, a
                opcode = "1110110101000111"
                size = 2
            elif op1 == "r":  # ld r, a
                opcode = "1110110101001111"
                size = 2
            elif mode1 == "label" and is_indirect[0] and is_first_pass:
                size = 3
            else:
                azze.invalid_operand(op1)
        # 16 bit operations
        elif op1 in ss:
            operands, ops_types, is_indirect = parser.detect_addressing_mode(
                operands, True)
            if op1 == "hl":
                if mode2 == "numeric" and is_indirect[1]:  # ld hl, (nn)
                    opcode = "00101010" + op2[1:-1]
                    size = 3
            elif op1 == "sp":  # ld sp, hl
                if op2 == "hl":
                    opcode = "11111001"
                    size = 1
                elif op2 == "ix":  # ld sp, ix
                    opcode = "1101110111111001"
                    size = 2
                elif op2 == "iy":  # ld sp, iy
                    opcode = "1111110111111001"
                    size = 2
            if mode2 == "numeric" and not is_indirect[1]:  # ld ss, nn
                print 'aqui', operands
                opcode = "00" + ss[op1] + "0001" + op2
                size = 3
            elif mode2 == "numeric" and is_indirect[1] and op1 != "hl":
                # ld ss, (nn)
                opcode = "1110110101" + ss[op1] + "1011" + op2[1:-1]
                size = 4
            elif mode2 == "label" and is_first_pass:
                if op1 == "hl" and is_indirect[1]:
                    size = 3
                elif is_indirect[1]:
                    size = 4
                elif not is_indirect[1]:
                    size = 2
            else:
                azze.invalid_operand(op2)
        elif op1 == "ix":
            operands, ops_types, is_indirect = parser.detect_addressing_mode(
                operands, True)
            if mode2 == "numeric" and not is_indirect[1]:  # ld ix, nn
                opcode = "1101110100100001" + op2
                size = 4
            elif mode2 == "numeric" and is_indirect[1]:
                opcode = "1101110100101010" + op2[1:-1]  # ld ix, (nn)
                size = 4
            elif mode2 == "label" and is_first_pass:
                size = 4
            else:
                azze.invalid_operand(op2)
        elif op1 == "iy":
            operands, ops_types, is_indirect = parser.detect_addressing_mode(
                operands, True)
            if mode2 == "numeric" and not is_indirect[1]:  # ld iy, nn
                opcode = "1111110100100001" + op2
                size = 3
            elif ops_types[1] == "numeric" and is_indirect[1]:  # ld iy, (nn)
                opcode = "1111110100101010" + op2[1:-1]
                size = 4
            elif mode2 == "label" and is_first_pass:
                if not is_indirect[1]:
                    size = 3
                elif is_indirect[1]:
                    size = 4
            else:
                azze.invalid_operand(op2)
        elif mode1 == "numeric" and is_indirect[0]:
            operands, ops_types, is_indirect = parser.detect_addressing_mode(
                operands, True)
            if op2 == "hl":  # ld (nn), hl
                opcode = "00100010" + op1[-1:1]
                size = 3
            elif op2 in ss:  # ld (nn), ss
                opcode = "1110110101" + ss[op2] + "0011" + op1[1:-1]
                size = 4
            elif op2 == "ix":  # ld (nn), ix
                opcode = "1101110100100010" + op1[1:-1]
                size = 4
            elif op2 == "iy":  # ld (nn), iy
                opcode = "1111110100100010" + op1[1:-1]
                size = 4
        elif mode1 == "label" and is_indirect[0] and is_first_pass:
            if op2 == "hl":
                size = 3
            elif op2 in ss or op2 == "ix" or op2 == "iy":
                size = 4
            else:
                azze.invalid_operand(op2)
        else:
            azze.invalid_operand(op1)
        return opcode, size


def push(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("push")
    elif op_count > 1:
        azze.many_args("push")
    else:  # addressing modes: qq, ix, iy
        op1 = operands[0]
        if op1 in qq:
            opcode = "11" + qq[op1] + "0101"
            size = 1
        elif op1 == "ix":
            opcode = "1101110111100101"
            size = 2
        elif op1 == "iy":
            opcode = "1111110111100101"
            size = 2
        else:
            azze.invalid_operand(op1)
    return opcode, size


def pop(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count < 1:
        azze.few_args("pop")
    elif op_count > 1:
        azze.many_args("pop")
    else:
        op1 = operands[0]
        if op1 in qq:
            opcode = "11" + qq[op1] + "0001"
            size = 1
        elif op1 == "ix":
            opcode = "1101110111100001"
            size = 2
        elif op1 == "iy":
            opcode = "1111110111100001"
            size = 2
        else:
            azze.invalid_operand(op1)
    return opcode, size

    # Exchange, Block Transfer & Block Search Group


def ex(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count < 2:
        azze.few_args("ex")
    elif op_count > 2:
        azze.many_args("ex")
    else:
        op1 = operands[0]
        op2 = operands[1]
        if op1 == "de" and op2 == "hl":
            opcode = "11101011"
            size = 1
        elif op1 == "af" and op2 == "af'":
            opcode = "00001000"
            size = 1
        elif op1 == "(sp)":
            if op2 == "hl":
                opcode = "11100011"
                size = 1
            elif op2 == "ix":
                opcode = "1101110111100011"
                size = 2
            elif op2 == "iy":
                opcode = "1111110111100011"
                size = 2
            else:
                azze.invalid_operand(op2)
        else:
            azze.invalid_operand(op1)
    return opcode, size


def exx(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count > 0:
        azze.many_args("exx")
    else:
        opcode = "11011001"
        size = 1
    return opcode, size


def ldi(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count > 0:
        azze.many_args("ldi")
    else:
        opcode = "1110110110100000"
        size = 2
    return opcode, size


def ldir(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count > 0:
        azze.many_args("ldir")
    else:
        opcode = "1110110110110000"
        size = 2
    return opcode, size


def ldd(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count > 0:
        azze.many_args("ldd")
    else:
        opcode = "1110110110101000"
        size = 2
    return opcode, size


def lddr(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count > 0:
        azze.many_args("lddr")
    else:
        opcode = "1110110110111000"
        size = 2
    return opcode, size


def cpi(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count > 0:
        azze.many_args("cpi")
    else:
        opcode = "1110110110100001"
        size = 2
    return opcode, size


def cpir(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count > 0:
        azze.many_args("cpir")
    else:
        opcode = "1110110110110001"
        size = 2
    return opcode, size


def cpd(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count > 0:
        azze.many_args("cpd")
    else:
        opcode = "1110110110101001"
        size = 2
    return opcode, size


def cpdr(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count > 0:
        azze.many_args("cpdr")
    else:
        opcode = "1110110110111001"
        size = 2
    return opcode, size

    # Logic & Arithmetic Group


def additions(operands, is_first_pass, change="000"):
    opcode = ""
    size = ""
    operands, ops_types, is_indirect = parser.detect_addressing_mode(operands)
    op1 = operands[0]
    if op1 in r:
        opcode = "10" + change + r[op1]
        size = 1
    elif ops_types[0] == "numeric" and not is_indirect[0]:
        opcode = "11" + change + "110" + op1
        size = 2
    elif op1 == "(hl)":
        opcode = "10" + change + "110"
        size = 1
    elif op1[:4] == "(ix+":
        opcode = "1101110110" + change + "110" + parser.get_displacement(op1)
        size = 3
    elif op1[:4] == "(iy+":
        opcode = "1111110110" + change + "110" + parser.get_displacement(op1)
        size = 3
    elif ops_types[0] == "label" and not is_indirect[0] and is_first_pass:
        size = 2
    else:
        azze.invalid_operand(op1)
    return opcode, size


def add(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("add")
    elif op_count > 2:
        azze.many_args("add")
    else:
        op1 = operands[0]
        op2 = operands[1]
        if op1 == "hl" and op2 in ss:
                opcode = "00" + ss[op2] + "1001"
                size = 1
        elif op1 == "ix" and op2 in pp:
                opcode = "1101110100" + pp[op2] + "1001"
                size = 2
        elif op1 == "iy" and op2 in rrr:
                opcode = "1111110100" + rrr[op2] + "1001"
                size = 2
        else:
            opcode, size = additions(operands, is_first_pass, "000")
    return opcode, size


def adc(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("adc")
    elif op_count > 2:
        azze.many_args("adc")
    else:
        op1 = operands[0]
        op2 = operands[1]
        if op1 == "hl":
            if op2 in ss:
                opcode = "1110110101" + ss[op2] + "010"
                size = 2
        else:
            opcode, size = additions(operands, is_first_pass, "001")
    return opcode, size


def sub(operands, op_count, is_first_pass):
    if op_count < 1:
        azze.few_args("sub")
    elif op_count > 1:
        azze.many_args("sub")
    else:
        return additions(operands, is_first_pass, "010")


def sbc(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("sbc")
    elif op_count > 2:
        azze.many_args("sbc")
    else:
        op1 = operands[0]
        op2 = operands[1]
        if op1 == "hl":
            if op2 in ss:
                opcode = "1110110101" + ss[op2] + "0010"
                size = 2
        else:
            opcode, size = additions(operands, is_first_pass, "011")
    return opcode, size


def zand(operands, op_count, is_first_pass):
    if op_count < 1:
        azze.few_args("and")
    elif op_count > 1:
        azze.many_args("and")
    else:
        return additions(operands, is_first_pass, "100")


def zor(operands, op_count, is_first_pass):
    if op_count < 1:
        azze.few_args("or")
    elif op_count > 1:
        azze.many_args("or")
    else:
        return additions(operands, is_first_pass, "110")


def zxor(operands, op_count, is_first_pass):
    if op_count < 1:
        azze.few_args("xor")
    elif op_count > 1:
        azze.many_args("xor")
    else:
        return additions(operands, is_first_pass, "101")


def cp(operands, op_count, is_first_pass):
    if op_count < 1:
        azze.few_args("cp")
    elif op_count > 1:
        azze.many_args("cp")
    else:
        return additions(operands, is_first_pass, "111")


def increments(operands, is_first_pass, change="100"):
    opcode = ""
    size = ""
    op1 = operands[0]
    if op1 in r:
        opcode = "00" + r[op1] + change
        size = 1
    elif op1 == "(hl)":
        opcode = "00" + "110" + change
        size = 1
    elif op1[:4] == "(ix+":
        opcode = "1101110100110" + change + parser.get_displacement(
            op1)
        size = 3
    elif op1[:4] == "(iy+":
        opcode = "1111110100110" + change + parser.get_displacement(
            op1)
        size = 3
    else:
        azze.invalid_operand(op1)
    return opcode, size


def inc(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("inc")
    elif op_count > 1:
        azze.many_args("inc")
    else:
        op1 = operands[0]
        if op1 in ss:
            opcode = "00" + ss[op1] + "0011"
            size = 1
        elif op1 == "ix":
            opcode = "1101110100100011"
            size = 2
        elif op1 == "iy":
            opcode = "1111110100100011"
            size = 2
        else:
            opcode, size = increments(operands, is_first_pass, "100")
    return opcode, size


def dec(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("dec")
    elif op_count > 1:
        azze.many_args("dec")
    else:
        op1 = operands[0]
        if op1 in ss:
            opcode = "00" + ss[op1] + "1011"
            size = 1
        elif op1 == "ix":
            opcode = "1101110100101011"
            size = 2
        elif op1 == "iy":
            opcode = "1111110100101011"
            size = 2
        else:
            opcode, size = increments(operands, is_first_pass, "101")
    return opcode, size

    # General Purpose Arithmetic & CPU Control


def daa(operands, op_count, is_first_pass):
    opcode = "00100111"
    size = 1
    if op_count > 0:
        azze.many_args("daa")
    return opcode, size


def cpl(operands, op_count, is_first_pass):
    opcode = "00101111"
    size = 1
    if op_count > 0:
        azze.many_args("cpl")
    return opcode, size


def neg(operands, op_count, is_first_pass):
    opcode = "1110110101000100"
    size = 2
    if op_count > 0:
        azze.many_args("neg")
    return opcode, size


def ccf(operands, op_count, is_first_pass):
    opcode = "00111111"
    size = 1
    if op_count > 0:
        azze.many_args("ccf")
    return opcode, size


def scf(operands, op_count, is_first_pass):
    opcode = "00110111"
    size = 1
    if op_count > 0:
        azze.many_args("scf")
    return opcode, size


def nop(operands, op_count, is_first_pass):
    opcode = "00000000"
    size = 1
    if op_count > 0:
        azze.many_args("nop")
    return opcode, size


def halt(operands, op_count, is_first_pass):
    opcode = "01110110"
    size = 1
    if op_count > 0:
        azze.many_args("halt")
    return opcode, size


def di(operands, op_count, is_first_pass):
    opcode = "11110011"
    size = 1
    if op_count > 0:
        azze.many_args("di")
    return opcode, size


def ei(operands, op_count, is_first_pass):
    opcode = "11111011"
    size = 1
    if op_count > 0:
        azze.many_args("ei")
    return opcode, size


def im(operands, op_count, is_first_pass):
    opcode = ""
    size = ""
    if op_count < 1:
        azze.few_args("adc")
    elif op_count > 1:
        azze.many_args("adc")
    else:
        op1 = operands[0]
        if op1 == "00000000":
            opcode = "1110110101000110"
            size = 2
        elif op1 == "00000001":
            opcode = "1110110101010110"
            size = 2
        elif op1 == "00000010":
            opcode = "1110110101011110"
            size = 2
        else:
            azze.invalid_operand(op1)
    return opcode, size

    # Rotate & Shift


def rlca(operands, op_count, is_first_pass):
    opcode = "00000111"
    size = 1
    if op_count > 0:
        azze.many_args("rlca")
    return opcode, size


def rla(operands, op_count, is_first_pass):
    opcode = "00010111"
    size = 1
    if op_count > 0:
        azze.many_args("rla")
    return opcode, size


def rrca(operands, op_count, is_first_pass):
    opcode = "00001111"
    size = 1
    if op_count > 0:
        azze.many_args("rrca")
    return opcode, size


def rra(operands, op_count, is_first_pass):
    opcode = "00011111"
    size = 1
    if op_count > 0:
        azze.many_args("rra")
    return opcode, size


def rotate(operands, op_count, is_first_pass, change="000"):
    opcode = "1100101100"
    size = ""
    op1 = operands[0]

    if op1 in r:
        opcode = opcode + change + r[op1]
        size = 2
    elif op1 == "(hl)":
        opcode = opcode + change + "110"
        size = 2
    elif op1[:4] == "(ix+":
        opcode = "11011101" + opcode + parser.get_displacement(op1) + "00" + \
                 change + "110"
        size = 4
    elif op1[:4] == "(ix+":
        opcode = "11111101" + opcode + parser.get_displacement(op1) + "00" + \
                 change + "110"
        size = 4
    else:
        azze.invalid_operand(op1)
    return opcode, size


def rlc(operands, op_count, is_first_pass):
    if op_count > 1:
        azze.many_args("rlc")
    elif op_count < 1:
        azze.few_args("rlc")
    return rotate(operands, op_count, is_first_pass, "000")


def rrc(operands, op_count, is_first_pass):
    if op_count > 1:
        azze.many_args("rrc")
    elif op_count < 1:
        azze.few_args("rrc")
    return rotate(operands, op_count, is_first_pass, "001")


def rl(operands, op_count, is_first_pass):
    if op_count > 1:
        azze.many_args("")
    elif op_count < 1:
        azze.few_args("")
    return rotate(operands, op_count, is_first_pass, "010")


def rr(operands, op_count, is_first_pass):
    if op_count > 1:
        azze.many_args("rr")
    elif op_count < 1:
        azze.few_args("rr")
    return rotate(operands, op_count, is_first_pass, "011")


def sla(operands, op_count, is_first_pass):
    if op_count > 1:
        azze.many_args("sla")
    elif op_count < 1:
        azze.few_args("sla")
    return rotate(operands, op_count, is_first_pass, "100")


def sra(operands, op_count, is_first_pass):
    if op_count > 1:
        azze.many_args("srl")
    elif op_count < 1:
        azze.few_args("srl")
    return rotate(operands, op_count, is_first_pass, "101")


def srl(operands, op_count, is_first_pass):
    if op_count > 1:
        azze.many_args("srl")
    elif op_count < 1:
        azze.few_args("srl")
    return rotate(operands, op_count, is_first_pass, "111")


def rld(operands, op_count, is_first_pass):
    opcode = "1110110101101111"
    size = 2
    if op_count > 0:
        azze.many_args("rld")
    return opcode, size


def rrd(operands, op_count, is_first_pass):
    opcode = "1110110101100111"
    size = 2
    if op_count > 0:
        azze.many_args("rrd")
    return opcode, size

    # Bit Set, Reset & Test


def bit_addressing(operands, is_first_pass, change="01"):
    bits = ("000", "001", "010", "011", "100", "101", "110", "111")
    op1 = operands[0][5:8]  # we have already converted this to binary
    op2 = operands[1]
    opcode = "11001011"
    size = 0
    if op1 in bits:
        if op2 in r:
            opcode = opcode + change + op1 + r[op2]
            size = 2
        elif op2 == "(hl)":
            opcode = opcode + change + op1 + "110"
            size = 2
        elif op2[:4] == "(ix+":
            opcode = "11011101" + opcode + parser.get_displacement(op1) + "00" + \
                     change + op1 + "110"
            size = 4
        elif op2[:4] == "(ix+":
            opcode = "11111101" + opcode + parser.get_displacement(op1) + "00" + \
                     change + op1 + "110"
            size = 4
        else:
            azze.invalid_operand(op2)
    elif not is_first_pass:
        azze.invalid_operand(op1)
    else:
        if op2 in r or op2 == "(hl)":
            size = 2
        elif op2[:4] == "(ix+" or op2[:4] == "(ix+":
            size = 4
        else:
            azze.invalid_operand(op2)
    return opcode, size


def bit(operands, op_count, is_first_pass):
    if op_count > 2:
        azze.many_args("bit")
    elif op_count < 2:
        azze.few_args("bit")
    return bit_addressing(operands, is_first_pass, "01")


def res(operands, op_count, is_first_pass):
    if op_count > 2:
        azze.many_args("res")
    elif op_count < 2:
        azze.few_args("res")
    return bit_addressing(operands, is_first_pass, "10")


def zset(operands, op_count, is_first_pass):
    if op_count > 2:
        azze.many_args("set")
    elif op_count < 2:
        azze.few_args("set")
    return bit_addressing(operands, is_first_pass, "11")

    # Jump Group


def jp(operands, op_count, is_first_pass):
    # mnemonic that takes 1 or 2 args
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("jp")
    elif op_count > 2:
        azze.many_args("jp")
    else:
        operands, ops_types, is_indirect = parser.detect_addressing_mode(
            operands)
        op1 = operands[0]
        if ops_types[0] == "numeric" and not is_indirect[0]:
            opcode = "11000011" + op1
            size = 3
        elif op1 in cc:
            try:
                op2 = operands[1]
                optype = ops_types[1]
                indirect = is_indirect[1]
            except IndexError:
                op2 = None
                optype = None
                indirect = None
                azze.few_args("jp")
            if optype == "numeric" and not indirect:
                opcode = "11" + cc[op1] + "010" + op2
                size = 3
            elif optype == "label" and is_first_pass and not indirect:
                size = 3
            else:
                azze.invalid_operand(op2)
        elif op1 == "(hl)":
            opcode = "11101001"
            size = 1
        elif op1 == "(ix)":
            opcode = "1101110111101001"
            size = 2
        elif op1 == "(iy)":
            opcode = "1111110111101001"
            size = 2
        elif ops_types[0] == "label" and not is_indirect[0] and is_first_pass:
            size = 3
        else:
            azze.invalid_operand(op1)
    return opcode, size


def jr(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("jr")
    elif op_count > 2:
        azze.many_args("jr")
    else:
        operands, ops_types, is_indirect = parser.detect_addressing_mode(
            operands)
        op1 = operands[0]
        if ops_types[0] == "numeric" and not is_indirect[0]:
            op1 = bin(int(op1, 2) - 2)[2:].zfill(8)  # op1 = op1 - 2
            opcode = "00011000" + op1
            size = 2
        elif ops_types[0] == "label" and not is_indirect[0] and is_first_pass:
            size = 2
        else:
            try:
                op2 = operands[1]
                optype = ops_types[1]
                indirect = is_indirect[1]
            except IndexError:
                op2 = None
                optype = None
                indirect = None
                azze.few_args("jp")
            if optype == "numeric" and not indirect:
                op2 = bin(int(op2, 2) - 2)[2:].zfill(8)  # op2 = op2 - 2
                if op1 == "c":
                    opcode = "00111000" + op2
                    size = 2
                elif op1 == "nc":
                    opcode = "00110000" + op2
                    size = 2
                elif op1 == "z":
                    opcode = "00101000" + op2
                    size = 2
                elif op1 == "nz":
                    opcode = "00100000" + op2
                    size = 2
            elif optype == "label" and not indirect and is_first_pass:
                size = 2
            else:
                azze.invalid_operand(op2)
    return opcode, size


def djnz(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("djnz")
    elif op_count > 1:
        azze.many_args("djnz")
    else:
        operands, ops_types, is_indirect = parser.detect_addressing_mode(
            operands)
        if ops_types[0] == "numeric" and not is_indirect[0]:
            op1 = bin(int(operands[0], 2) - 2)[2:].zfill(8)  # op1 = op1 - 2
            opcode = "00010000" + op1
            size = 2
        if ops_types[0] == "label" and not is_indirect[0] and is_first_pass:
            size = 2
        else:
            azze.invalid_operand(operands[0])
    return opcode, size

    # Call & Return Group


def call(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("call")
    elif op_count > 2:
        azze.many_args("call")
    else:
        operands, ops_types, is_indirect = parser.detect_addressing_mode(
            operands)
        op1 = operands[0]
        if ops_types[0] == "numeric" and not is_indirect[0]:
            opcode = "11001101" + op1
            size = 3
        elif ops_types[0] == "label" and not is_indirect[0] and is_first_pass:
            size = 3
        else:
            try:
                op2 = operands[1]
                optype = ops_types[1]
                indirect = is_indirect[1]
            except IndexError:
                op2 = None
                optype = None
                indirect = None
                azze.few_args("jp")
            if optype == "numeric" and not indirect:
                if op1 in cc:
                    opcode = "11" + cc[op1] + "100" + op2
                    size = 3
            elif optype == "label" and not indirect and is_first_pass:
                size = 2
            else:
                azze.invalid_operand(op2)
    return opcode, size


def ret(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 0:
        azze.few_args("ret")
    elif op_count > 1:
        azze.many_args("ret")
    else:
        if not operands:
            opcode = "11001001"
            size = 1
        elif operands[0] in cc:
            opcode = "11" + cc[operands[0]] + "000"
            size = 3
        else:
            azze.invalid_operand(operands[0])
    return opcode, size


def reti(operands, op_count, is_first_pass):
    opcode = "1110110101001101"
    size = 2
    if op_count > 0:
        azze.many_args("reti")
    return opcode, size


def retn(operands, op_count, is_first_pass):
    opcode = "1110110101000101"
    size = 2
    if op_count > 0:
        azze.many_args("retn")
    return opcode, size


def rst(operands, op_count, is_first_pass):
    opcode = ""
    size = 0
    if op_count < 1:
        azze.few_args("rst")
    elif op_count > 1:
        azze.many_args("rst")
    else:
        op1 = operands[0]
        if op1 in p:
            opcode = "11" + p[op1] + "111"
            size = 1
        elif is_first_pass:
            size = 1
        else:
            azze.invalid_operand(op1)
    return opcode, size

    # Azzembler Directives


def db(operands, op_count, is_first_pass):
    bin_op_list = []
    opcode = ""
    size = 0
    if op_count < 0:
        azze.few_args("db")
    elif op_count > 16:
        azze.many_args("db")
    else:
        operands, ops_types, is_indirect = parser.detect_addressing_mode(
            operands)
        for i in range(0, len(operands)):
            if ops_types[i] != "numeric":
                for char in operands[i]:
                    bin_op_list.append(bin(ord(char))[2:].zfill(8))
                    size += 1
            else:
                bin_op_list.append(operands[i])
                length = len(operands[i]) / 8
                size += length
        opcode = "".join(bin_op_list)
    return opcode, size


def dw(operands, op_count, is_first_pass):
    bin_op_list = []
    opcode = ""
    size = 0
    if op_count < 0:
        azze.few_args("dw")
    elif op_count > 16:
        azze.many_args("dw")
    else:
        operands, ops_types, is_indirect = parser.detect_addressing_mode(
            operands)
        for i in range(0, len(operands)):
            if ops_types[i] != "numeric":
                azze.bad_symbol(operands)
            else:
                operands[i] = operands[i].zfill(16)
                bin_op_list.append(operands[i])
                length = len(operands[i]) / 8
                size += length
        opcode = "".join(bin_op_list)
    return opcode, size


# Opcodes mapping

mnemonics = {
    # Load Group
    "ld": ld, "push": push, "pop": pop,
    # Exchange, Block Transfer & Block Search Group
    "ex": ex, "exx": exx, "ldi": ldi, "ldir": ldir, "ldd": ldd, "lddr": lddr,
    "cpi": cpi, "cpir": cpir, "cpd": cpd, "cpdr": cpdr,
    # Logic & Arithmetic Group
    "add": add, "adc": adc, "sub": sub, "sbc": sbc, "and": zand, "or": zor,
    "xor": zxor,
    "cp": cp, "inc": inc, "dec": dec,
    # General Purpose Arithmetic & CPU Control
    "daa": daa, "cpl": cpl, "neg": neg, "ccf": ccf, "scf": scf, "nop": nop,
    "halt": halt, "di": di, "ei": ei, "im": im,
    # Rotate & Shift
    "rlca": rlca, "rla": rla, "rrca": rrca, "rra": rra, "rlc": rlc, "rl": rl,
    "rrc": rrc, "rr": rr, "sla": sla, "sra": sra, "srl": srl, "rld": rld,
    "rrd": rrd,
    # Bit Set, Reset & Test
    "bit": bit, "set": zset, "res": res,
    # Jump Group
    "jp": jp, "jr": jr, "djnz": djnz,
    # Call & Return Group
    "call": call, "ret": ret, "reti": reti, "retn": retn, "rst": rst,
    # I/O Group
    # "in": zin, "ini": ini, "inir": inir, "ind": ind, "indr": indr, "out": out,
    # "outi": outi, "outir": outir, "outd": outd, "outdr": outdr,
    # Azzembler Directives
    "db": db, "dw": dw
}
