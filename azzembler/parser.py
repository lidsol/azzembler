#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016 Emilio Cabrera
#
# GNU GENERAL PUBLIC LICENSE
#    Version 3, 29 June 2007
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import azz_error as azze
import azz_globals as azzg
import mnemonics as azzm


def clean_line(line):
    """Cleans the line and returns a lists with the operands.

    Removes comments, tabs, commas, semi-colons and splits the instruction,
    operands and labels

    Args:
        line (str): Line of the assembly file

    Returns:
        instruction (list): List with instruction, operands and labels if any
    """
    line = line.strip()
    if not line:
        return
    line = line.split(";")[0]  # remove comments
    line = line.expandtabs().replace(",", " ")
    if '"' in line:
        temp = line.replace('"', " ").split()
        line = []
        for i in range(0, len(temp)):
            if temp[i].lower() == "db":
                line.append(temp[i].lower())
                line = line + temp[(i + 1):]
                break
            else:
                line.append(temp[i].lower())
    else:
        line = line.lower().split()
    instruction = check_brackets(line)
    return instruction


def check_brackets(instruction):
    """Verifies the use of parenthesis.

    Checks parenthesis matching, joins in a single list element the operand
    using parenthesis, and verifies that only one operand is using
    parenthesis since memory to memory movements are not allowed in the Z80

    Args:
        instruction (list): List of operands with wrong parenthesis matching

    Returns:
        temp_instruction (lists): Joined operands

    """
    brackets = False
    concat = False
    matching_n = 0
    i = 0
    temp_op = []
    temp_instruction = []
    if not instruction:
        return
    for element in instruction:
        if "(" in element:
            if element[0] != "(":  # parenthesis must be at the beginning of
                #  the string
                azze.bad_symbol(element)
            if brackets and not concat:  # we already find and complete a
                # couple of brackets
                azze.too_many_brackets()  # we are not supposed to find more
                # than one
            if brackets or matching_n:  # we were supposed to find and
                # closing bracket
                azze.too_many_brackets()
            brackets = True  # we have find a starting bracket
            concat = True    # we must concatenate till we find the closing
            # bracket
            if ")" in element:  # is the closing bracket in the same element
                # of the list?
                if element[-1] != ")":  # closing
                    # parenthesis must be at the
                    # end of the string
                    azze.bad_symbol(element)
                matching_n += 1  # we have 1 matching couple
                concat = False  # so we don have to concatenate anything
                temp_instruction.append(element)  # add to the temporal
                # instruction holder
                continue
        if concat:  # do we have to concatenate?
            temp_op.append(element)  # add the current element to the list
            # for concat
        else:
            temp_instruction.append(element)  # we don't have to do nothing
            # just keep this in the instruction
        if ")" in element:  # is this the closing element?
            if not brackets or not concat:  # we haven't find a opening bracket
                azze.no_opening_bracket()
            if element[-1] != ")":  # closing parenthesis must be at the
                # end of the string
                azze.bad_symbol(element)
            temp_op = "".join(temp_op)
            temp_instruction.append(temp_op)  # concatenate and add to the
            # temporal instruction holder
            matching_n += 1  # increase the number of matching pairs
            concat = False  # we don't have to concat anymore
        i += 1
    return temp_instruction


def get_label(instruction):
    """Saves a label in the label dictionary and removes the label definition.

    Checks if the instruction list has any label declaration on it and
    appends it to the label dictionary.

    Do not checks if the labels has been already defined, thus allowing label
    redefinition at any point in the program

    Args:
        instruction (list): List of operands in the instruction

    Returns:
        instruction (list): List of operands with label definition removed if any

    """
    if not instruction:  # ignore empty elements
        return
    label = instruction[0]
    if label[-1] == ":":  # finding label definitions
        label = label[:-1]
        if label in azzm.v_ops or instruction[0][-1] in azzm.mnemonics:
            azze.invalid_label(label)
        azzg.labels[label] = azzg.lcount
        del instruction[0]  # delete label definition
    return instruction


def get_operands(instruction):
    """Returns the operands of an insruction list

    Args:
        instruction (list): list including the mnemonic and the operands

    Returns:
        operands (list): list of operands, empty if no arguments
        (int): count of operands

    """
    operands = [operand for operand in instruction]
    del operands[0]  # delete the instruction, just leave the operand(s)
    return operands, len(operands)


def replace_labels(operands):
    """Replaces the labels in the operands by their location counter value
    according to the labels dictionay

    Args:
        operands (list): List of operands

    Returns:
        temp (list): Operands replaced by their value (2 bytes, in binary)

    """
    temp = []  # instruction holder
    for operand in operands:
        if operand in azzg.labels:  # finding labels in direct addressing mode
            value, num = parse_num(str(azzg.labels[operand]), True, False)
            temp.append("0b" + value)
        elif operand[0] == "(" and operand[1:-1] in azzg.labels:
            # labels in indirect mode
            value, num = parse_num(str(azzg.labels[operand[1:-1]]), True, False)
            temp.append("(" + "0b" + value + ")")
        elif len(operand) > 4 and operand[3] == "+" and operand[4:-1] in \
                azzg.labels:
            # indexed addressing mode
            value, num = parse_num(str(azzg.labels[operand[4:-1]]), True, False)
            temp.append(operand[:4] + "0b" + value + ")")
        else:
            temp.append(operand)  # no label, keep the operand
    return temp


def parse_num(operand, _16bits=False, to_little_endian=True):
    """Returns the operand and True if the value is numeric, False otherwise

    Note: this function does not detect numeric values in indexed addressing
    modes, see get_displacement

    Args:
        operand (str): An operand in a instructions
        _16bits (bool): Indicates if the returned value must be 16 bits long
        to_little_endian: Indicates if the returned value must be in little
            endian
    Returns:
        operands (str): String with numeric values replaced
        is_num (bool): True if the operand is a numeric value.
    """
    is_num = False
    try:
        if operand[0:2] == "0x":  # detecting hexadecimal operand
            _16bits = True if (len(operand[2:]) >= 3 and not _16bits) else False
            operand = bin(int(operand, 16))[2:]
        elif operand[0:2] == "0b":  # detecting binary operand
            _16bits = True if (len(operand[2:]) >= 9 and not _16bits) else False
            operand = operand[2:]
        elif operand[0] == "0o":  # detecting octal operand
            _16bits = True if (len(operand[2:]) >= 4 and not _16bits) else False
            operand = bin(int(operand, 8))[2:]
        else:
            operand = bin(int(operand))[2:]  # converting decimal values
        if len(operand) > 8:
            _16bits = True
        operand = operand.zfill(16) if _16bits else operand.zfill(8)
        if _16bits and to_little_endian:  # to little endian
            operand = operand[8:] + operand[0:8]
        is_num = True
    except TypeError:
        is_num = False
    except ValueError:
        is_num = False
    return operand, is_num


def detect_addressing_mode(operands, _16_bits=False):
    """Detects the type of addressing in the operands.

    Also converts the numeric values to binary by calling parse_num()

    Args:
        operands (list): List of operands.

    Returns:
        operands (list): List of operands with numeric values replaced by its
            binary form.
        ops_types (list): List with the type of operands: "register",
        "numeric" or "label"
        is_indirect (list): List of boolean values indicating the adressing mode

    """
    ops_types = []
    is_indirect = []
    temp = []
    for operand in operands:
        if operand[0] == "(":
            is_indirect.append(True)
            operand = operand[1:-1]
            _16_bits = True  # indirect mode addressing requires 16 bit
            # directions
        else:
            is_indirect.append(False)
        if operand in azzm.v_ops:
            ops_types.append("register")
        elif operand in azzg.labels:
            ops_types.append("label")
        else:
            operand, is_num = parse_num(operand, _16_bits)
            if is_num:
                ops_types.append("numeric")
            else:
                ops_types.append("label")  # maybe a label not yet defined
        if is_indirect[-1]:
            operand = "(" + operand + ")"
        temp.append(operand)
    return temp, ops_types, is_indirect


def get_displacement(operand):
    """Returns the binary value of the displacement in indexed addressing mode.

    Args:
        operand (str): Operand to evaluate

    Returns:
        (str): Binary form of the displacement in the operand

    """
    return parse_num(operand[4:-1], False)[0]







