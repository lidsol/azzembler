#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2016 Emilio Cabrera
#
# GNU GENERAL PUBLIC LICENSE
#    Version 3, 29 June 2007
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import azz_globals as azzg
import azz_error as azze
from mnemonics import mnemonics as mnemonics


def command_parse():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("-f", "--file", help="Archivo de codigo fuente "
                                                "para ensamblar")
    argparser.add_argument("-l", "--lst",  help="Archivo lst, si se omite no "
                                                "se generará el archivo lst")
    argparser.add_argument("-x", "--hex",  help="Ejecutable ya ensamblado, "
                                                "si se omite se usará el "
                                                "mismo nombre que el archivo "
                                                "de entrada")
    return argparser.parse_args()


# TODO: on first pass don't stop on errors, keep a list of errors and display
#  them at the end of the pass
def first_pass(asm):
    for line in asm:
        azzg.line_num += 1
        lst = {"linenum": azzg.line_num, "lcount": azzg.lcount, "code": "",
               "size": 0, "line": line}
        azzg.lst[azzg.line_num] = lst
        instruction = parser.clean_line(line)
        if not instruction:  # ignore if empty
            continue
        instruction = parser.get_label(instruction)
        if not instruction:  # ignore if empty
            continue
        operands, operands_len = parser.get_operands(instruction)
        operands = parser.replace_labels(operands)
        opcode, size = mnemonics.get(
            instruction[0], azze.bad_symbol
        )(operands, operands_len, True)
        if opcode is None and size is None:
            continue
        lst["code"] = opcode
        lst["size"] = size
        lst["lcount"] = azzg.lcount
        azzg.lst[azzg.line_num] = lst
        azzg.instructions[azzg.line_num] = [instruction[0]] + operands
        azzg.bindump[azzg.line_num] = opcode
        azzg.lcount = azzg.lcount + size
    print
    return


def second_pass():
    azzg.line_num = 0
    for i in azzg.lst:
        azzg.line_num += 1
        if azzg.lst[i]["code"] == "" and azzg.lst[i]["size"] != 0:
            instruction = azzg.instructions[i]
            operands, operands_len = parser.get_operands(instruction)
            operands = parser.replace_labels(operands)
            opcode, size = mnemonics.get(
                instruction[0], azze.bad_symbol
            )(operands, operands_len, False)
            if opcode is None and size is None:
                continue
            azzg.lst[i]["code"] = opcode
            azzg.bindump[i] = opcode
        if len(azzg.lst[i]["code"]) / 8 != azzg.lst[i]["size"]:
            azze.wrong_operand_len(azzg.instructions[i])


def to_hex(bindump):
    tmp = []
    count = 0
    hexdump = ""
    hexlines = []
    for i in bindump:
        if bindump[i] != "":
            tmp.append(bindump[i])
    bindump = "".join(tmp)
    while bindump:
        dataint = int(bindump[:4], 2)
        hexdump += hex(dataint)[2:]
        bindump = bindump[4:]
        if len(hexdump) == 32 or bindump == "":
            checksum = 0
            hexline = hex(len(hexdump) / 2)[2:].zfill(2) + \
                hex(16 * count)[2:].zfill(4) + "00" + hexdump
            for i in range(0, len(hexline), 2):
                checksum += int(hexline[i] + hexline[i+1], 16)
            checksum = (checksum ^ 0xFF) + 1
            hexlines.append(hexline + hex(checksum)[2:][-2:].zfill(2))
            hexdump = ""
            count += 1
    hexlines.append("00000001FF")
    return hexlines


def main(args):
    args = command_parse()
    if args.file:
        with open(args.file, "rb+") as asm:
            first_pass(asm)
            # TODO: check if the error list is empty, if not display errors
            # and finish execution
            asm.close()
        second_pass()
        hexlines = to_hex(azzg.bindump)
        if args.hex:
            hexfile = args.hex
        else:
            hexfile = args.file.split(".asm")[0] + ".hex"
        import codecs
        with codecs.open(hexfile, "wb+", encoding='ascii') as hexf:
            for line in hexlines:
                line = ":" + line + "\r\n"
                hexf.write(line.upper().encode('ascii'))
            hexf.write("\r\n".encode('ascii'))
            hexf.close()
        if args.lst:
            with open(args.lst, "wb+") as lst:
                line = []
                for i in azzg.lst:
                    if azzg.lst[i]["code"] != "":
                        if azzg.lst[1]["size"] < 3:
                            line.append("%s\t%s\t%s\t%s\t\t%s" % (
                                str(azzg.lst[i]["linenum"]).zfill(3),
                                hex(azzg.lst[i]["lcount"])[2:].zfill(4),
                                str(azzg.lst[i]["size"]),
                                hex(int(azzg.lst[i]["code"], 2))[2:].zfill(
                                    azzg.lst[i]["size"] * 2),
                                azzg.lst[i]["line"],
                            ))
                        else:
                            line.append("%s\t%s\t%s\t%s\t%s" % (
                                str(azzg.lst[i]["linenum"]).zfill(3),
                                hex(azzg.lst[i]["lcount"])[2:].zfill(4),
                                str(azzg.lst[i]["size"]),
                                hex(int(azzg.lst[i]["code"], 2))[2:].zfill(
                                    azzg.lst[i]["size"] * 2),
                                azzg.lst[i]["line"],
                            ))
                    else:
                        line.append("%s\t%s\t%s\t%s\t\t%s" % (
                            str(azzg.lst[i]["linenum"]).zfill(3),
                            hex(azzg.lst[i]["lcount"])[2:].zfill(4),
                            str(azzg.lst[i]["size"]),
                            "    ",
                            azzg.lst[i]["line"],
                        ))
                lst.write("".join(line).expandtabs())
                lst.close()
            print "Ensamble exitoso"
    else:
        print azz_error.NO_ASM_FILE
    return 0


if __name__ == "__main__":
    import sys
    import argparse
    import azz_error
    import parser
sys.exit(main(sys.argv))
