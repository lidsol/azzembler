#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2016 Emilio Cabrera
#
# GNU GENERAL PUBLIC LICENSE
#    Version 3, 29 June 2007
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from sys import exit
import azz_globals as azzg

# TODO: add a parameter to choose whenever to print or not the errors
# TODO: change the error methods to return a error code instead of die
# TODO: create a dictionary to map error codes to corresponding functions

NO_ASM_FILE = """Debes proporcionar un archivo para ensamblar
Intenta azzembler.py -h para más información
"""


def bad_symbol(symbol, dummy = "", useless = True):
    print "En la línea", azzg.line_num, ": simbolo desconocido:", symbol
    return None, None
    # die()


def too_many_brackets():
    print "En la línea", azzg.line_num, ": demasiados parentesis en la instrucción"
    return None, None


def no_closing_bracket():
    print "En la línea", azzg.line_num, ": no hay parentesis de cierre"
    return None, None


def no_opening_bracket():
    print "En la línea", azzg.line_num, ": no hay parentesis de apertura"
    return None, None


def many_args(inst):
    print "En la línea", azzg.line_num, ": demasiados argumentos para", inst
    return None, None


def few_args(inst):
    print "En la línea", azzg.line_num, ": faltan argumentos para", inst
    return None, None


def empty_braces():
    print "En la línea", azzg.line_num, ": los parentesis están vacíos"
    return None, None


def invalid_label(label):
    print "En la línea", azzg.line_num, ": etiqueta no válida:", label
    return None, None


def invalid_operand(operand):
    print "En la línea", azzg.line_num, ": operando inválido:", operand
    return None, None


def die():
    exit("Ensamble abortado debido a errores")


def invalid_addressing_mode(mnemonic, operand):
    print "En la línea", azzg.line_num, ": modo de direccionamiento erroneo " \
                                        "para:", mnemonic, ":", operand
    return None, None


def wrong_operand_len(instruction):
    print "En la línea", azzg.line_num, ": longitud del operando errónea:", \
        instruction


def invalid_string_use(instruction):
    print "En la línea", azzg.line_num, ": no se esperaba una cadena:", \
        instruction